<!-- Header Starts -->
@if($client)
     @if($client->hamburger_status == 1 || $client->member_logo_status == 1)
        <div class="manu-sec">
            <div class="mob-menu-box">
                @if($client->hamburger_status == 1)
                    <div class="menu-icon"><span></span> <p>menu</p></div>
                @endif
                @if($client->member_logo_status == 1)
                    <div class="mamber-logo"><img src="{{asset('storage/'.$client->member_logo)}}"></div>
                @endif
            </div>
        </div>
    @endif
@endif
<div class="mob-menu-up">
    <div class="mob-menu">
        <div class="mob-close-box"><span class="mob-close"><img src="img/close-white.png" alt="img"></span>
        </div>
        <ul class="navigation">
            <li><a href="login.html">Log In</a></li>

        </ul>
    </div>
</div>

  <header class="">
            <div class="side-space">
                <section class="container">
                    <div class="row">
                        <div class="col-12  ">
                            <div class="bgC">
                                <div class="logo" style="padding"><img src="{{asset('/img/YUPELRI_QSA_logo.png')}}">
                                </div>
                              
                                <div class="indication">


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </header>
<!-- Header Ends -->

