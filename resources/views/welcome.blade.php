@section('title')
Yupelri QSA – PLUGIN
@endsection
@section('css')
@endsection
@extends('layouts.layout')
@section('content')
<!-- Main Section -->
<form class="form" method="POST" action="{{route('pdfMerge')}}" target="_blank|_self|_parent|_top|framename"> 
    @csrf



    <section class="side-space main-section">
        <section class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="top-link ">
                        <ul>
                            <li class = "pglink"><a href="#provider">Provider  </a></li>
                           
                            <li class = "pglink"><a href="#patients">Patient</a></li>
                           
                            <li><a href="https://dailymed.nlm.nih.gov/dailymed/fda/fdaDrugXsl.cfm?type=display&setid=6dfebf04-7c90-436a-9b16-750d3c1ee0a6" target="_blank" class="gaClick" data-label="PI">Prescribing Information</a></li>
                           
                            <li><a href="#isi"  class="gaClick" data-scroll data-label="ISI">Important Safety Information</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-12">
                    <nav class="notification">
                        <ul>
                            <li><img src="{{asset('img/select.png')}}"> Select tool(s)</li>
                            <li><img src="{{asset('img/search.png')}}"> Preview tool</li>
                            <li><img src="{{asset('img/arrow.png')}}"> Description</li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="row headings " id="provider">
                <div class="col-6">
                    <div class="mr-left fsdfs">
                      
                    </div>
                </div>
                {{-- <div class="col-6">
                    <small>*These resources cannot be printed.</small>
                </div> --}}
            </div>
           
            {{-- Provider pdf --}}
            <div class="row ">
                <div class="col-12 ">
                    <div class="pd-left">
                        <div class="acc pd-left">
                             <h3 class="fys">PROVIDER</h3>
                            @foreach( $resources as $resource )
                                @if( $resource->menu_type == 1 && $resource->category_type == 1 )
                                    @php
                                       
                                        if($resource->separate_pi == "1")
                                        {
                                            $pi_pdf = json_decode($resource->pdf_with_pi);
                                        }
                                        else
                                        {
                                             $pdf = json_decode($resource->pdf);
                                        }
                                    @endphp
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="providerResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}" 
                                                data-title="{{$resource->title}}"
                                                data-english_description="{{$resource->english_description}}"
                                                data-spanish_description="{{$resource->spanish_description}}"
                                               
                                                    @if($resource->separate_pi == 1)
                                                        data-tool-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                        data-print-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                        data-title="{{$resource->title}}"
                                                        data-can-print="{{$resource->can_print}}"
                                                   @elseif(!empty($pdf[0]->download_link))
                                                        data-tool-url="{{$resource->cms_link}}"
                                                        data-print-url="{{$resource->cms_link}}"
                                                        data-title="{{$resource->title}}"
                                                        data-can-print="{{$resource->can_print}}"
                                                    @endif
                                               
                                                >
                                               <!--  -->
                                            
                                                <label for="{{$resource->id}}">{{$resource->title}}</label>
                                                    
                                            </span>
                                            @if($resource->cms_link)
                                                <a href="{{$resource->cms_link}}" target="_blank" class="preview">
                                                        <img src="{{asset('img/search.png')}}">
                                                </a>
                                            @else
                                                @if($resource->separate_pi == 1)
                                                     <a href="{{asset('storage/'.$pi_pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                @elseif(!empty($pdf[0]->download_link))
                                                    <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                @endif
                                            @endif
                                            <a class="drop-down">
                                                <img src="{{asset('img/arrow.png')}}">
                                            </a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                           {{$resource->english_description}}
                                           @if($resource->can_print == 0)
                                            @if($resource->no_print_title == 1)
                                               <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                            @endif
                                           @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>





            {{-- provider fillable forms --}}
          

            <div class="row ">
                <div class="col-12 ">
                    <div class="pd-left">
                        <div class="acc pd-left">
                            <!--<div class="fil">Fillable Forms</div>-->
                             @foreach( $resources as $resource )
                                @if( $resource->menu_type == 1 && $resource->category_type == 8)
                                    @php
                                       
                                        if($resource->separate_pi == "1")
                                        {
                                            $pi_pdf = json_decode($resource->pdf_with_pi);
                                        }
                                       
                                    @endphp
                                    
                                    <div class="acc__card">
                                         
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="providerFillableForms[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                data-english_description="{{$resource->english_description}}"
                                                data-spanish_description="{{$resource->spanish_description}}"
                                                 data-title="{{$resource->title}}"
                                                 @if($resource->separate_pi == 0)
                                                   
                                                    data-tool-url="{{$resource->cms_link}}"
                                                    data-print-url="{{$resource->cms_link}}"
                                                   
                                                    data-can-print="{{$resource->can_print}}"
                                                @elseif(!empty($pdf[0]->download_link) )
                                                    data-tool-url=""
                                                    data-print-url=""
                                                   
                                                    data-can-print="{{$resource->can_print}}"
                                                @elseif($resource->cms_link)
                                                    
                                                @endif
                                                >
                                           
                                            </span>
                                            @if($resource->separate_pi == 1)
                                              
                                            @elseif($resource->cms_link)
                                               
                                            @else
                                             @if(!empty($pdf[0]->download_link))
                                               
                                             @endif
                                            @endif
                                           
                                        </div>
                                       
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            
            @foreach( $resources as $resource )
                @if($resource->menu_type == 1 && $resource->category_type == 3)
                 <div class="row mr-top1">
                        <div class="col-12  mr-left">
                            <div class="pd-left sub-title">
                                <h3>Video</h3>

                                <aside class="points ">
                                    <ul class="">
                                        <li><span>1</span>Viewed<br>
                                            online</li>
                                        <li><span>2</span>You can email<br>
                                            or copy the link</li>
                                    </ul>
                                </aside>

                            </div>
                        </div>
                    </div>

                  
               
                    <div class="row ">
                        <div class="col-12 ">
                            <div class="pd-left">
                                <div class="acc pd-left">
                                    @foreach( $resources as $resource )
                                        @if( $resource->menu_type == 1 && $resource->category_type == 3 )
                                            <div class="acc__card">
                                                <div class="acc__title">
                                                    <span class="form-group">
                                                        <input type="checkbox" name="VideoResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                        data-can-print="{{$resource->can_print}}" data-title="{{$resource->title}}"  @if(!empty($resource->video_link))
                                                        data-tool-url="{{$resource->video_link}}"
                                                        
                                                         @elseif($resource->redirect_to_cms == 1)
                                                         data-tool-url="{{$resource->cms_link}}"
                                                         @endif
                                                        >
                                                        <label for="{{$resource->id}}">{{$resource->title}}
                                                        @if($resource->can_print == 0)
                                                            @if($resource->no_print_title == 1)
                                                                <small class="small-info">PLEASE NOTE: This is a video. It
                                                                    cannot be printed.</small>
                                                            @endif
                                                        @endif
                                                        </label>
                                                    </span>
                                                    @if($resource->redirect_to_cms == 1)
                                                        <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                <img src="{{asset('img/search.png')}}">
                                                        </a>
                                                    @else
                                                    @if(!empty($resource->video_link))
                                                        <a href="{{$resource->video_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                            <img src="{{asset('img/search.png')}}">
                                                        </a>
                                                     @endif
                                                    @endif
                                                    {{-- <a class="preview"><img src="{{asset('img/search.png')}}"></a> --}}
                                                    <a class="drop-down"><img src="{{asset('img/arrow.png')}}"></a>
                                                </div>
                                                <div class="acc__panel  mr-left">
                                                    {{$resource->english_description}}
                                                    @if($resource->can_print == 0)
                                                        @if($resource->no_print_title == 1)
                                                            <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
            {{-- video ends here --}}
        </section>
    </section>


   

   

                          <section class="container bg-grey">
                         <div class="education-grey">
                            Educational resources for patients
                  
                        </div>
                         </section>

    <section class="side-space main-section mr-top2 ">
        <section class="container">
            <div id="tab-1" class="tab-content current">
                <div class="row headings " id="patients">
                    <div class="col-md-8">
                        <div class="mr-left gfdf" id="dfa">
                     
                        </div>
                    </div>
                </div>
         

                <div class="row">
                    <div class="col-12 ">
                        <div class="pd-left">
                            <div class="acc pd-left">
                                 <h3 class="fys1">PATIENT</h3>
                                @foreach( $resources as $resource )
                                    @if( $resource->menu_type == 2 && $resource->category_type == 1 )
                                        @php
                                        if($resource->separate_pi == 1)
                                        {

                                            $pi_pdf = json_decode($resource->pdf_with_pi);
                                        }
                                        else
                                        {
                                            $pdf = json_decode($resource->pdf);
                                        }
                                        @endphp
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="CPResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                    data-english_description="{{$resource->english_description}}"
                                                    data-spanish_description="{{$resource->spanish_description}}"
                                                     data-title="{{$resource->title}}"
                                                    @if($resource->separate_pi == 1)
                                                        data-tool-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                        data-print-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                       
                                                        data-can-print="{{$resource->can_print}}"
                                                    @elseif(!empty($pdf[0]->download_link))
                                                        data-tool-url="{{$resource->cms_link}}"
                                                        data-print-url="{{$resource->cms_link}}"
                                                       
                                                        data-can-print="{{$resource->can_print}}"
                                                    @elseif($resource->cms_link)
                                                        data-tool-url = "{{$resource->cms_link}}"
                                                        data-print-url = "{{$resource->cms_link}}"
                                                        data-can-print="{{$resource->can_print}}"
                                                    @endif
                                                    >
                                                    <label for="{{$resource->id}}">{{$resource->title}}
                                                    </label>
                                                    @if($resource->can_print == 0)
                                                        @if($resource->no_print_title == 1)
                                                          <!--   <small class="small-info">PLEASE NOTE: This is an online form
                                                            that allows you to download the card.</small> -->
                                                        @endif
                                                    @endif
                                                </span>
                                                @if($resource->cms_link)
                                                    <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                            <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                @else
                                                @if($resource->separate_pi ==1)
                                                    <a href="{{asset('storage/'.$pi_pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                 @elseif(!empty($pdf[0]->download_link))
                                                    <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                 @endif
                                                @endif
                                                {{-- <a class="preview"><img src="{{asset('img/search.png')}}"></a> --}}
                                                <a class="drop-down"><img src="{{asset('img/arrow.png')}}"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                {!!$resource->english_description!!}
                                                <br>
                                                <br>
                                         
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                 {{-- fillable forms --}}
                @foreach( $resources as $resource )
                    @if($resource->menu_type == 2 && $resource->category_type == "2" )
                        <div class="row mr-top1">
                            <div class="col-12  mr-left">
                                <div class="pd-left sub-title">
                                    <h3>Fillable Forms*</h3>

                                    <aside class="points ">
                                        <ul class="">
                                            <li><span>1</span>Fill in <br>
                                                form</li>
                                            <li><span>2</span>Print and/or<br>
                                                save/submit</li>
                                            <li><span>3</span>You can email<br>
                                                or copy the link</li>
                                        </ul>
                                    </aside>
                                </div>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-12 ">
                                <div class="pd-left">
                                    <div class="acc pd-left">
                                        @foreach( $resources as $resource )
                                            @if( $resource->menu_type == 2 && $resource->category_type == 2 )
                                                @php
                                                if($resource->separate_pi==1)
                                                {
                                                    $pi_pdf = json_decode($resource->pdf_with_pi);
                                                }
                                                else
                                                {

                                                    $pdf = json_decode($resource->pdf);
                                                }
                                                @endphp
                                                <div class="acc__card">
                                                    <div class="acc__title">
                                                        <span class="form-group">
                                                            <input type="checkbox" name="providerFillableForms[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                            data-english_description="{{$resource->english_description}}"
                                                            data-spanish_description="{{$resource->spanish_description}}"
                                                            @if($resource->separate_pi == 1)
                                                                data-tool-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                                data-print-url="{{asset('storage/'.$pi_pdf[0]->download_link)}}"
                                                                data-title="{{$resource->title}}"
                                                                data-can-print="{{$resource->can_print}}"
                                                            @elseif(!empty($pdf[0]->download_link))
                                                                data-tool-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                                data-print-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                                data-title="{{$resource->title}}"
                                                                data-can-print="{{$resource->can_print}}"
                                                            @endif
                                                            >
                                                            <label for="{{$resource->id}}">{{$resource->title}}*</label>
                                                        </span>
                                                        @if($resource->redirect_to_cms == 1)
                                                            <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                    <img src="{{asset('img/search.png')}}">
                                                            </a>
                                                        @else
                                                         @if($resource->separate_pi == 1)
                                                            <a href="{{asset('storage/'.$pi_pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                <img src="{{asset('img/search.png')}}">
                                                            </a>
                                                         @elseif(!empty($pdf[0]->download_link))
                                                            <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                                <img src="{{asset('img/search.png')}}">
                                                            </a>
                                                         @endif
                                                        @endif
                                                        <a class="drop-down"><img src="{{asset('img/arrow.png')}}"></a>
                                                    </div>
                                                    <div class="acc__panel  mr-left">
                                                       {{$resource->english_description}}
                                                       @if($resource->can_print == 0)
                                                        @if($resource->no_print_title == 1)
                                                           <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                                        @endif
                                                       @endif
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

                
                {{-- english video starts here  --}}
            
                <div class="row ">
                    <div class="col-12 ">
                        <div class="pd-left">
                            <div class="acc pd-left">
                                @foreach( $resources as $resource )
                                    @if( $resource->menu_type == 2 && $resource->category_type == 3 )
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="VideoResources[]" class="checkbox gacheckbox" id="{{$resource->id}}" value="{{$resource->id}}"
                                                    data-can-print="{{$resource->can_print}}"  
                                                    data-title="{{$resource->title}}"
                                                    data-english_description="{{$resource->english_description}}"
                                                     data-print-url="{{ $resource->video_link }}"  @if(!empty($resource->video_link))
                                                        data-tool-url="{{$resource->video_link}}"
                                                        
                                                         @elseif($resource->redirect_to_cms == 1)
                                                         data-tool-url="{{$resource->cms_link}}"
                                                         @endif
                                                    >

                                              
                                                </span>
                                                 @if($resource->cms_link == 1)
                                                    <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                            <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                @else
                                               @if(!empty($resource->video_link))
                                                    <a href="{{$resource->video_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                 @endif
                                                @endif
                                            
                                                <a class="drop-down"><img src="{{asset('img/arrow.png')}}"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                {{$resource->english_description}}
                                             
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                {{-- english video ends here  --}}
               
            </div>
            
            {{-- spanish tab starts here --}}
            <div id="tab-2" class="tab-content ">
                <div class="row headings ">
                    <div class="col-md-8">
                        <div class="mr-left">
                            <h3>Caregiver and Patient</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12  mr-left">
                        <div class="pd-left sub-title">
                            <h3>Resources</h3>
                        </div>
                    </div>
                </div>

                <div class="row ">
                    <div class="col-12 ">
                        <div class="pd-left">
                            <div class="acc pd-left">
                                @foreach( $resources as $resource )
                                    @if( $resource->menu_type == 2 && $resource->category_type == 1 )
                                        @php
                                            $pdf = json_decode($resource->pdf);
                                        @endphp
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="CPSpanishResources[]" class="checkbox gacheckbox" id="spanish{{$resource->id}}" value="{{$resource->id}}"
                                                    data-english_description="{{$resource->english_description}}"
                                                    data-spanish_description="{{$resource->spanish_description}}"
                                                    @if(!empty($pdf[0]->download_link))
                                                    data-tool-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                    data-print-url="{{asset('storage/'.$pdf[0]->download_link)}}"
                                                    data-title="{{$resource->title}}"
                                                    data-can-print="{{$resource->can_print}}"
                                                    @endif
                                                    >
                                                    <label for="spanish{{$resource->id}}">
                                                    {{$resource->title}}
                                                    @if($resource->can_print == 0)
                                                     @if($resource->no_print_title == 1)
                                                         <span class="note"> PLEASE NOTE: This tool can not be printed.</span>
                                                     @endif
                                                    @endif
                                                    </label>
                                                </span>
                                                 @if($resource->cms_link == 1)
                                                    <a href="{{$resource->cms_link}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                            <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                @else
                                                 @if(!empty($pdf[0]->download_link))
                                                    <a href="{{asset('storage/'.$pdf[0]->download_link)}}" target="_blank" class="preview gapreview" data-title="{{$resource->title}}">
                                                        <img src="{{asset('img/search.png')}}">
                                                    </a>
                                                 @endif
                                                @endif
                                                {{-- <a class="preview"><img src="{{asset('img/search.png')}}"></a>  --}}
                                                <a class="drop-down"><img src="{{asset('img/arrow.png')}}"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                {{$resource->english_description}}
                                             
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            {{-- spanish tab ends here --}}
            
            
             <a class="back-to-top" >
           <span style="color:#8c8b8b">Back to Top</span>  <img src="{{asset('img/ge.png')}}" class="bt">
        </a>
        </section>


         



            <section class="bg-c">
       
        
        <section class="container">
            <div class="row ">
                <div class="col-12 d-flex justify-content-center">
                    <a href="#" data-toggle="modal" data-target="#myModal" class="button gaEmail" id="email_res">Email</a>
                    <button class="button viewPrint" >View/Print</button>
                    <button class="button gaCopy" id="copy-tool">Copy Link</button>
                </div>
            </div>
        </section>
    </section>
      



                  <section class="para">
                    <div class="row ">
                        <div class="col-12 imp-safety">
                            <h6>Indication</h6>
                            <div class="hiz">YUPELRI<sup>®</sup> inhalation solution is indicated for the maintenance treatment of patients with chronic obstructive 
pulmonary disease (COPD).</div>
                            <h6 id="isi" class="head">Important Safety Information</h6>
                            <div class="hiz">YUPELRI is contraindicated in patients with hypersensitivity to revefenacin or any component of this product.<br></div>
    <div class="hiz">
    YUPELRI should not be initiated in patients during acutely deteriorating or potentially life-threatening episodes 
of COPD, or for the relief of acute symptoms, i.e., as rescue therapy for the treatment of acute episodes 
of bronchospasm. Acute symptoms should be treated with an inhaled short-acting beta<sub>2</sub> -agonist.</div>
    <div class="hiz">
        As with other inhaled medicines, YUPELRI can produce paradoxical bronchospasm that may be 
life-threatening. If paradoxical bronchospasm occurs following dosing with YUPELRI, it should be treated 
immediately with an inhaled, short-acting bronchodilator. YUPELRI should be discontinued immediately and 
alternative therapy should be instituted.
    </div>
    <div class="iga">YUPELRI should be used with caution in patients with narrow-angle glaucoma. Patients should be instructed 
to immediately consult their healthcare provider if they develop any signs and symptoms of acute 
narrow-angle glaucoma, including eye pain or discomfort, blurred vision, visual halos or colored images 
in association with red eyes from conjunctival congestion and corneal edema.</div>

<div class="mon">
   Worsening of urinary retention may occur. Use with caution in patients with prostatic hyperplasia 
or bladder-neck obstruction and instruct patients to contact a healthcare provider immediately 
if symptoms occur.
</div>
<div class="cdj">Immediate hypersensitivity reactions may occur after administration of YUPELRI. If a reaction occurs, 
YUPELRI should be stopped at once and alternative treatments considered.</div>

<div class="cdj">The most common adverse reactions occurring in clinical trials at an incidence greater than or equal to 2%
in the YUPELRI group, and higher than placebo, included cough, nasopharyngitis, upper respiratory infection, 
headache and back pain.</div>
<div class="pass">Coadministration of anticholinergic medicines or OATP1B1 and OATP1B3 inhibitors with YUPELRI 
is not recommended.</div>

<div class="pass">YUPELRI is not recommended in patients with any degree of hepatic impairment.</div>

<div class="cdj see">Please see full <a class="pre" target="_blank" href="https://dailymed.nlm.nih.gov/dailymed/fda/fdaDrugXsl.cfm?type=display&setid=6dfebf04-7c90-436a-9b16-750d3c1ee0a6">Prescribing Information</a> For additional information, please contact us at <a href="tel:1-800-395-3376">1-800-395-3376.</a></div>



</form>
<!-- Main Section Over-->

@endsection
@section('js')
<script type="text/javascript">
    function errorFunction(message) {
        event.preventDefault();
        $('#errorResponse').html(message);
        $('#errorResponse').fadeIn();
        setTimeout(function () { $('#errorResponse').fadeOut(); }, 3000);
    }
    var canPrintCheck = [];
    var cannotPrintCheck = [];
    var urls;
    var titles = [];
    var resource_ids = [];
    $(document).on('click', '.checkbox', function(event) {
    
        if (($(this).attr('data-can-print') == 1)) {
            if($(this).is(':checked')){
                console.log( ($(this).val()  )  );
                canPrintCheck.push($(this).val());    
            }else{
                var index = canPrintCheck.indexOf($(this).val());
               
            }
            
        }else{
             if($(this).is(':checked')){
                cannotPrintCheck.push($(this).val());    
            }else{
                var index = cannotPrintCheck.indexOf($(this).val());
                if (index !== -1) cannotPrintCheck.splice(index, 1);
            }
        }
       
        urls = ($(':checkbox:checked').map(function() {
             return $(this).attr('data-print-url');
           }).get());
        
        titles = ($(':checkbox:checked').map(function() {
             return $(this).attr('data-title') + ' ';
           }).get());
           
            if($(this).is(':checked'))
            {   resource_ids.push($(this).attr('id'));
                console.log($(this).attr('data-title'));
                var emailBody = '<div id="eb-'+$(this).attr('id')+'" class = "ebclass"><b class="hea-title">Here is a resource that might be of interest. Please open the link to view:<br><br>'+$(this).attr('data-title')+'</b><br>'+$(this).attr('data-english_description')+'<br> <a href="'+$(this).attr('data-print-url')+'">'+$(this).attr('data-print-url')+'</a> <br><br></div>';
                $('#email-body').append(emailBody);
                
            }
            else
            {
                var removeItem = $(this).attr('id');
              
                resource_ids = jQuery.grep(resource_ids, function(value) {
                  return value != removeItem;
                });
                
                $('#eb-'+$(this).attr('id')).remove();
               
            }

        $('#email-body-hidden').val($('#email-body').html());
                
        var count = $('#email-body .ebclass').length ; 
        if(count == 1)
        {
            $('#ebres1').show();
            $('#ebresmul').hide(); 
        }
        else if(count >= 2)
        {
           $('#ebres1').hide();
            $('#ebresmul').show(); 
        }

    });
    
    $(document).on('click', '.viewPrint', function(event) {
        if(cannotPrintCheck.length === 0){
            if (canPrintCheck.length === 0) {
               
            errorFunction('Please select at least 1 printable tool')

            }else{
                urls = ($(':checkbox:checked').map(function() {
                    canPrintCheck.push(($(this).attr('data-can-print')));
                   }).get());
                if($.inArray("0", canPrintCheck) !== -1){
                    errorFunction('Please unselect the link which cannot be printed');
                }
            }
        }else{
            errorFunction('Please uncheck non-printable tool');
        }
           
    });
    
    var checkBoxCategory = @if(empty($client)) 'plugin' @else '{{$client->name}}' @endif ;
   
    
    $(function(){
        $('#email_res').on('click',function(){
           $('#res_ids').val(resource_ids);
        });
    });
    
   
</script>
@endsection

